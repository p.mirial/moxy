package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"regexp"
)

func main() {
	//Init
	proxyUrl, _ := url.Parse("http://localhost:9000")
	myClient := &http.Client{Transport: &http.Transport{Proxy: http.ProxyURL(proxyUrl)}}
	rx := regexp.MustCompile(`The Zscaler proxy virtual IP is <span class="detailOutput">(.*?)</span>.</div>`)

	//Query ip page
	resp, err := myClient.Get("https://ip.zscaler.com")
	if err != nil {
		log.Fatal(err)
	}

	b, _ := io.ReadAll(resp.Body)
	stringyByte := string(b)

	//Extract and return
	result := rx.FindStringSubmatch(stringyByte)
	if len(result) != 2 {
		log.Fatal("couldn't correctly match proxy virtual IP")
	}
	fmt.Println(result[1])
}
