#!/bin/bash

FILED=~/.profile

grep -q -e 'HTTP_PROXY=' $FILED || echo 'export HTTP_PROXY=' >> $FILED
grep -q -e 'HTTPS_PROXY=' $FILED || echo 'export HTTPS_PROXY=' >> $FILED
grep -q -e 'http_proxy=' $FILED || echo 'export http_proxy=' >> $FILED
grep -q -e 'https_proxy=' $FILED || echo 'export https_proxy=' >> $FILED
sed -i 's/HTTP_PROXY=.*/HTTP_PROXY=/g' $FILED
sed -i 's/HTTPS_PROXY=.*/HTTPS_PROXY=/g' $FILED
sed -i 's/http_proxy=.*/http_proxy=/g' $FILED
sed -i 's/https_proxy=.*/https_proxy=/g' $FILED

if grep -q microsoft /proc/version; then
  echo "good"
else
  echo "native Linux, aint gonna do anything"
  return
fi

if [ -f "gowsl.exe" ]; then
  echo "detecting zscaler proxy..."
else
  echo "missing gowsl.exe, please run 'go.exe build .'"
  return
fi

PROXIP=$(./gowsl.exe)

if [ "$?" = "0" ]; then
  echo "recover proxy $PROXIP"
else
  source $FILED
  echo "error recovering proxy $?"
  return
fi

sed -i "s+HTTP_PROXY=.*+HTTP_PROXY=http://$PROXIP:80+g" $FILED
sed -i "s+HTTPS_PROXY=.*+HTTPS_PROXY=http://$PROXIP:80+g" $FILED

sed -i "s+http_proxy=.*+http_proxy=http://$PROXIP:80+g" $FILED
sed -i "s+https_proxy=.*+https_proxy=http://$PROXIP:80+g" $FILED

source $FILED
